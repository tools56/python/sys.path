"""
   Sys.path
   Affiche les chemins sytemes du  python
"""
import sys

def main():
    """
       Appel principale
    """
    bool_error=False
    for rep in sys.path:
        if rep.lower().find("user") >= 0:
            print("Warning !!!! "+rep)
            bool_error=True
        else  :
            print(rep)
    if bool_error :
        raise NameError('Warning')

if __name__ == '__main__':
    main()
